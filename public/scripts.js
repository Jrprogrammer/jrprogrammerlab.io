// Event listener for the scroll position which changes the menu item color;
document.addEventListener('scroll', function () {
    if (window.scrollY < 250) {
        const menu = document.getElementsByClassName('menuItem');
        for (let i = 0; i < menu.length; i++) {
            menu[i].style["color"] = "#535353"
        }
    }
    else if (window.scrollY < 1200 || window.scrollY > 2100) {
        const menu = document.getElementsByClassName('menuItem');
        for (let i = 0; i < menu.length; i++) {
            menu[i].style["color"] = "#FFFFFF"
        }
    }
    else if (window.scrollY > 1200) {
        const menu = document.getElementsByClassName('menuItem');
        for (let i = 0; i < menu.length; i++) {
            menu[i].style["color"] = "#535353"
        }
    }
});